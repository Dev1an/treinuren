//: [Previous](@previous)

import Foundation
import CoreLocation


let stationsJSON = JSON(data: NSData(contentsOfURL: NSURL(string: "https://irail.be/stations/NMBS")!)!)["@graph"]

var stations = [Station]()

for (_,subJson) in stationsJSON {
	stations.append(Station(id: subJson["name"].stringValue, name: subJson["@id"].stringValue, location: CLLocation(latitude: subJson["latitude"].doubleValue, longitude: subJson["longitude"].doubleValue)))
}

stations

//: [Next](@next)
