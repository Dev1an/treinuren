//: [Previous](@previous)

import CoreLocation

let stations = [
	Station(id: "http://irail.be/stations/NMBS/008896149", name: "Waregem", location: CLLocation(latitude: 50.892456, longitude: 3.42551)),
	Station(id: "http://irail.be/stations/NMBS/008896008", name: "Kortrijk", location: CLLocation(latitude: 50.824506, longitude: 3.264549)),
	Station(id: "http://irail.be/stations/NMBS/008891405", name: "Blankenberge", location: CLLocation(latitude: 51.312432, longitude: 3.133864)),
	Station(id: "http://irail.be/stations/NMBS/008833001", name: "Leuven", location: CLLocation(latitude: 50.88228, longitude: 4.715866)),
	Station(id: "http://irail.be/stations/NMBS/008814308", name: "Halle", location: CLLocation(latitude: 50.733931, longitude: 4.240634)),
	Station(id: "http://irail.be/stations/NMBS/008813003", name: "Brussel-Centraal/Bruxelles-Central", location: CLLocation(latitude: 50.845658, longitude: 4.356801)),
	Station(id: "http://irail.be/stations/NMBS/008813037", name: "Brussel-Kapellekerk/Bruxelles-Chapelle", location: CLLocation(latitude: 50.841127, longitude: 4.347866)),
	Station(id: "http://irail.be/stations/NMBS/008813045", name: "Brussel-Congres/Bruxelles-Congrès", location: CLLocation(latitude: 50.852067, longitude: 4.362051)),
	Station(id: "http://irail.be/stations/NMBS/008814001", name: "Brussel-Zuid/Bruxelles-Midi", location: CLLocation(latitude: 50.835707, longitude: 4.336531)),
	Station(id: "http://irail.be/stations/NMBS/008815040", name: "Brussel-West/Bruxelles-Ouest", location: CLLocation(latitude: 50.848552, longitude: 4.321042)),
	Station(id: "http://irail.be/stations/NMBS/008819406", name: "Brussel-Nat-Luchthaven/Bruxelles-Nat-Aéroport", location: CLLocation(latitude: 50.896456, longitude: 4.482075))
]

let villalaan = CLLocation(latitude: 50.729559, longitude: 4.249926)
let groepT = CLLocation(latitude: 50.874999, longitude: 4.707741)
let waregem = stations.first!.location

let reference = waregem

let sortedStations = stations.sort {$0.distanceFrom(reference)<$1.distanceFrom(reference)}
let nearbyStations = sortedStations[0...2].map {"\($0.name) (\(round($0.distanceFrom(reference))/1000)km)"}

nearbyStations

//: [Next](@next)
