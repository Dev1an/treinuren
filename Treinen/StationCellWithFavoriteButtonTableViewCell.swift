//
//  StationCellWithFavoriteButtonTableViewCell.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 10/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import UIKit

class StationCellWithFavoriteButtonTableViewCell: UITableViewCell {
	@IBOutlet var title: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
