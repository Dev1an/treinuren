//
//  ConstraintOperators.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 10/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation
import UIKit

infix operator -|- {}
infix operator |- {}
infix operator ÷ {}
infix operator |÷ {}
infix operator ÷| {}

func -|- (left: UIView, right: UIView) -> (multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
	return { multiplier, constant in
		let c = NSLayoutConstraint(item: left, attribute: .Trailing, relatedBy: .Equal, toItem: right, attribute: .Leading, multiplier: multiplier, constant: constant)
		c.priority = 50
		return c
	}
}

func |- (left: UIView, right: UIView) -> (multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
	return { multiplier, constant in
		let c = NSLayoutConstraint(item: left, attribute: .Leading, relatedBy: .Equal, toItem: right, attribute: .Leading, multiplier: multiplier, constant: constant)
		c.priority = 50
		return c
	}
}

func ÷ (left: UIView, right: UIView) -> (multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
	return { multiplier, constant in
		let c = NSLayoutConstraint(item: left, attribute: .Bottom, relatedBy: .Equal, toItem: right, attribute: .Top, multiplier: multiplier, constant: constant)
		c.priority = 50
		return c
	}
}

func |÷ (left: UIView, right: UIView) -> (multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
	return { multiplier, constant in
		let c = NSLayoutConstraint(item: left, attribute: .Top, relatedBy: .Equal, toItem: right, attribute: .Top, multiplier: multiplier, constant: constant)
		c.priority = 50
		return c
	}
}

func ÷| (left: UIView, right: UIView) -> (multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
	return { multiplier, constant in
		let c = NSLayoutConstraint(item: left, attribute: .Bottom, relatedBy: .Equal, toItem: right, attribute: .Bottom, multiplier: multiplier, constant: constant)
		c.priority = 50
		return c
	}
}