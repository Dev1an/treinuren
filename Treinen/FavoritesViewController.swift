//
//  FavoritesViewController.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 10/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import UIKit
import NmbsTouch
import CoreLocation

class FavoritesViewController: UITableViewController {
	
	var favorites: FilteredArray<Station> = []
	var remaining: FilteredArray<Station> = []
	
	let searchController = UISearchController(searchResultsController: nil)
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		searchController.searchResultsUpdater = self
		searchController.hidesNavigationBarDuringPresentation = false
		searchController.dimsBackgroundDuringPresentation = false
		searchController.searchBar.sizeToFit()
		tableView.tableHeaderView = searchController.searchBar
		
		setEditing(true, animated: false)
		
		nmbsManager.whenStationsAreAvailable {
			print("reloading data")
			if let favoriteStationIds = preferences.objectForKey("favoriteStations") as? [String] {
				self.favorites = FilteredArray(array: nmbsManager.stations(WithIds: favoriteStationIds))
			}
			var remainingStations = nmbsManager.stations
			for favorite in self.favorites.base {
				if let index = remainingStations.indexOf(favorite) {
					remainingStations.removeAtIndex(index)
				}
			}
			self.remaining = FilteredArray(array: remainingStations)
			self.tableView.reloadData()
		}
	}
	
	func saveToPreferences() {
		let favoriteIds = favorites.base.map {$0.id}
		preferences.setObject(favoriteIds, forKey: "favoriteStations")
		do {
			try watchConnection.updateApplicationContext(["favoriteStations": favoriteIds])
		} catch {
			print("unable to send favorites to apple watch")
		}
	}
	
	func addToFavorites(indexPath: NSIndexPath) {
		favorites.append(remaining.removeAtIndex(indexPath.row))
		tableView.moveRowAtIndexPath(indexPath, toIndexPath: NSIndexPath(forRow: favorites.count-1, inSection: 0))
	}
	
	func removeFromFavorites(indexPath: NSIndexPath) {
		tableView.moveRowAtIndexPath(indexPath, toIndexPath: NSIndexPath(forRow: remaining.sortedInsert(favorites.removeAtIndex(indexPath.row), isOrderedBefore: <)!, inSection: 1))
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return section == 0 ? "Favorieten" : "Overige stations"
	}

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		switch section {
			case 0: return favorites.count
			case 1: return remaining.count
			default: return 0
		}
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("stationCell", forIndexPath: indexPath)
		cell.showsReorderControl = true
		
		if indexPath.section == 0 {
			cell.textLabel?.text = favorites[indexPath.row].name
		} else {
			cell.textLabel?.text = remaining[indexPath.row].name
		}

        return cell
    }
	
	override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
		return indexPath.section == 0 ? .Delete : .Insert
	}
	
	override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return indexPath.section == 0
	}
	
	override func tableView(tableView: UITableView, targetIndexPathForMoveFromRowAtIndexPath sourceIndexPath: NSIndexPath, toProposedIndexPath proposedDestinationIndexPath: NSIndexPath) -> NSIndexPath {
		if proposedDestinationIndexPath.section != 0 {
			return NSIndexPath(forRow: favorites.count-1, inSection: 0)
		} else {
			return proposedDestinationIndexPath
		}
	}
	
	override func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
		favorites.move(fromIndex: sourceIndexPath.row, toIndex: destinationIndexPath.row)
		saveToPreferences()
	}
	
//	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//		return indexPath.section == 0
//	}

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
			let newIndexPath = NSIndexPath(forRow: remaining.sortedInsert(favorites.removeAtIndex(indexPath.row), isOrderedBefore: <)!, inSection: 1)
			tableView.beginUpdates()
			tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Middle)
			tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Middle)
			tableView.endUpdates()
        } else if editingStyle == .Insert {
			addToFavorites(indexPath)
        }
		saveToPreferences()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FavoritesViewController: UISearchResultsUpdating {
	func updateSearchResultsForSearchController(searchController: UISearchController) {
		if searchController.searchBar.text! == "" {
			favorites.filter = nil
			remaining.filter = nil
		} else {
			favorites.filter = {$0.name.lowercaseString.containsString(searchController.searchBar.text!.lowercaseString)}
			remaining.filter = favorites.filter
		}
		tableView.reloadData()
	}
}

extension Array {
	func insertionIndexOf(elem: Element, isOrderedBefore: (Element, Element) -> Bool) -> Int {
		var lo = 0
		var hi = self.count - 1
		while lo <= hi {
			let mid = (lo + hi)/2
			if isOrderedBefore(self[mid], elem) {
				lo = mid + 1
			} else if isOrderedBefore(elem, self[mid]) {
				hi = mid - 1
			} else {
				return mid // found at position mid
			}
		}
		return lo // not found, would be inserted at position lo
	}
	
	mutating func sortedInsert(newElement: Element, isOrderedBefore: (Element, Element) -> Bool) -> Int {
		let index = insertionIndexOf(newElement, isOrderedBefore: isOrderedBefore)
		self.insert(newElement, atIndex: index)
		return index
	}
}

func delay(delay:Double, closure:()->()) {
	dispatch_after(
		dispatch_time(
			DISPATCH_TIME_NOW,
			Int64(delay * Double(NSEC_PER_SEC))
		),
		dispatch_get_main_queue(), closure)
}