//
//  File.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 11/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation

class FilteredArray<Element where Element: Equatable>: ArrayLiteralConvertible {
    var base: Array<Element>
	var filteredCache: [Element]
	var filter: (Element -> Bool)? {
		didSet {
			if let filter = filter { filteredCache = base.filter(filter) }
			else { filteredCache = base }
		}
	}
	var filterActive: Bool { return filter != nil }
	
	init(array: [Element]) {
		base = array
		filteredCache = base
	}
	
	required convenience init(arrayLiteral elements: Element...) {
		self.init(array: elements)
	}
	
	subscript(index: Int) -> Element {
		get {
			return (filterActive ? filteredCache : base)[index]
		}
	}
	
	func move(fromIndex sourceIndex: Int, toIndex destinationIndex: Int) {
		let element = filteredCache.removeAtIndex(sourceIndex)
		filteredCache.insert(element, atIndex: destinationIndex)
		
		base.removeAtIndex(base.indexOf(element)!)
		let baseDestinationIndex: Int
		if destinationIndex == 0 {
			baseDestinationIndex = 0
		} else {
			baseDestinationIndex = base.indexOf(filteredCache[destinationIndex-1])!+1
		}
		base.insert(element, atIndex: baseDestinationIndex)
	}
	
	func sortedInsert(newElement: Element, isOrderedBefore: (Element, Element) -> Bool) -> Int? {
		let baseIndex = base.sortedInsert(newElement, isOrderedBefore: isOrderedBefore)
		if let filter = filter {
			if filter(newElement) {
				return filteredCache.sortedInsert(newElement, isOrderedBefore: isOrderedBefore)
			} else {
				return nil
			}
		} else {
			filteredCache.insert(newElement, atIndex: baseIndex)
			return baseIndex
		}
	}
	
	func append(newElement: Element) {
		base.append(newElement)
		if let filter = filter {
			if filter(newElement) {filteredCache.append(newElement)}
		} else {
			filteredCache.append(newElement)
		}
	}
	
	func removeAtIndex(index: Int) -> Element {
		let element = filteredCache.removeAtIndex(index)
		base.removeAtIndex(base.indexOf(element)!)
		return element
	}
	
	var count: Int {
		return (filterActive ? filteredCache : base).count
	}
}
