//
//  Date operators.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 21/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation

func > (left: NSDate, right: NSDate) -> Bool {
	return left.compare(right) == .OrderedDescending
}

func < (left: NSDate, right: NSDate) -> Bool {
	return left.compare(right) == .OrderedAscending
}