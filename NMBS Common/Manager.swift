//
//  Manager.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 8/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation
import CoreLocation

let browser = NSURLSession.sharedSession()

public class Manager: NSObject {
	// MARK: Initiation
	
	public var stations = [Station]()
	public var stationsQueue = dispatch_group_create()
	public var nearbyStationsQueue = dispatch_group_create()
	let urlTimeFormatter = NSDateFormatter()

	let finder = NSFileManager.defaultManager()
	let gps = CLLocationManager()
	var lastLocation: CLLocation?

	let stationsCacheDirectoryPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0]
	let stationscacheFilePath: String
	public var onStationsLoaded: (()->())?
	
	public override init() {
		stationscacheFilePath = "\(stationsCacheDirectoryPath)/stations.bin"
				super.init()
		urlTimeFormatter.dateFormat = "HHmm"
		
		loadStations()
		configureGPS()
	}
	
	// MARK: - Station loading
	func loadStations() {
		dispatch_group_enter(stationsQueue)
		dispatch_group_enter(nearbyStationsQueue)
		ensureCacheFileExists()
		
		if finder.fileExistsAtPath(stationscacheFilePath) {
			loadStationsFromCache(notifyStationsLoaded)
		} else {
			downloadStationsFromInternet(notifyStationsLoaded)
		}
	}
	
	func notifyStationsLoaded() {
		print("\(stations.count) stations loaded")
		dispatch_group_leave(stationsQueue)
		dispatch_group_leave(nearbyStationsQueue)
		if let location = lastLocation {
			setNearbyStations(location)
		}
		onStationsLoaded?()
	}
	
	func ensureCacheFileExists() {
		// Check whether a cache directory already exists
		if !finder.fileExistsAtPath(stationsCacheDirectoryPath) {
			// Make a cache directory
			do {
				try finder.createDirectoryAtPath(stationsCacheDirectoryPath, withIntermediateDirectories: true, attributes: nil)
			} catch {
				print("An error occured while creating the caches directory")
			}
		}
	}
	
	func loadStationsFromCache(callback: (()->())) {
		if let stationsCache = NSKeyedUnarchiver.unarchiveObjectWithData(NSData(contentsOfFile: stationscacheFilePath)!) as? [Station] {
			stations = stationsCache
			print("reading from cache")
			callback()
		}
	}
	
	func downloadStationsFromInternet(callback: (()->())) {
		browser.dataTaskWithURL(NSURL(string: "https://irail.be/stations/NMBS")!) { (data, response, error) in
			if let error = error {
				print("Error: \(error)")
			} else if let data = data {
				self.stations = []
				let stationsJSON = JSON(data: data)["@graph"]
				
				for (_,subJson) in stationsJSON {
					self.stations.append(Station(id: subJson["@id"].stringValue, name: subJson["name"].stringValue, location: CLLocation(latitude: subJson["latitude"].doubleValue, longitude: subJson["longitude"].doubleValue)))
				}

				self.stations.sortInPlace()
				
				print("NMBS train stations downloaded from internet")
				
				NSKeyedArchiver.archivedDataWithRootObject(self.stations).writeToFile(self.stationscacheFilePath, atomically: true)
				callback()
			} else {
				print("Problem while downloading NMBS train stations")
			}
		} .resume()
	}
	
	public func whenStationsAreAvailable(callback: ()->()) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
			dispatch_group_wait(self.stationsQueue, DISPATCH_TIME_FOREVER)
			dispatch_async(dispatch_get_main_queue(), callback)
		}
	}
	
	public func whenNearbyStationsAreAvailable(callback: ()->()) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
			dispatch_group_wait(self.nearbyStationsQueue, DISPATCH_TIME_FOREVER)
			dispatch_async(dispatch_get_main_queue(), callback)
		}
	}
	
	// MARK: Find stations by id
	public func station(withId id: String) -> Station? {
		if let index = stations.indexOf({ $0.id == id }) {
			return stations[index]
		}
		return nil
	}
	
	public func stations(WithIds ids: [String]) -> [Station] {
		var favoriteStations = [Station]()
		for id in ids {
			if let station = station(withId: id) {
				favoriteStations.append(station)
			}
		}
		return favoriteStations
	}
	
	// MARK: Find nearby stations
	public var numberOfNearbyStations = 5
	public var getNearbyStations: (ArraySlice<Station> -> Void)?
	
	func setNearbyStations(location: CLLocation) {
		let sorted = stations.sort {$0.distanceFrom(location) < $1.distanceFrom(location)}
		let slice = sorted[0..<self.numberOfNearbyStations]
		getNearbyStations?(slice)
	}
	
	public func updateLocation() {
		print("Core location service: \(CLLocationManager.locationServicesEnabled())")
		switch CLLocationManager.authorizationStatus() {
			case .Denied: print("Denied")
			case .Restricted: print("Restricted")
			
			case .NotDetermined: gps.requestWhenInUseAuthorization()
			case .AuthorizedAlways, .AuthorizedWhenInUse: gps.requestLocation()
		}
	}
	
	// MARK: Calculate Routes
	func timeForUrl() -> String {
		return urlTimeFormatter.stringFromDate(NSDate())
	}
	
	public func getTrains(from startPoint: Station, to endPoint: Station, callback: ([Connection]) -> ()) -> NSURLSessionDataTask {

		let url = "https://api.irail.be/connections/?to=\(endPoint.id)&from=\(startPoint.id)&format=JSON&time=\(timeForUrl())"
		
		let downloadRoute = browser.dataTaskWithURL(NSURL(string: url)!) { (data, response, error) in
			if let error = error {
				print("Error: \(error)")
			} else if let data = data {
				
				var connections = [Connection]()
				let connectionsList = JSON(data: data)["connection"]
				
				for (_,connection) in connectionsList {
					let start = connection["departure"]
					let end = connection["arrival"]
					let viaCountString = connection["vias"]["number"].string
					connections.append(
						Connection(
							startStation: startPoint,
							startTime: start["time"].doubleValue,
							startDelay: start["delay"].doubleValue,
							startPlatform: start["platform"].stringValue,
							
							endStation: endPoint,
							endTime: end["time"].doubleValue,
							endDelay: end["delay"].doubleValue,
							endPlatform: end["platform"].stringValue,
							viaCount: viaCountString == nil ? 0 : Int(viaCountString!)!
						)
					)
					print("\(connection["vias"]["number"]) \(connection["vias"]["number"].string)")
				}
				
				callback(connections)
			} else {
				print("Problem while fetching train connections")
			}
		}
		downloadRoute.resume()
		return downloadRoute
	}
}

// MARK: CoreLocation extension
extension Manager: CLLocationManagerDelegate {
	func configureGPS() {
		dispatch_group_enter(nearbyStationsQueue)
		gps.delegate = self
		gps.desiredAccuracy = kCLLocationAccuracyKilometer
	}
	
	public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let location = locations.last {
			if lastLocation == nil {dispatch_group_leave(nearbyStationsQueue)}
			lastLocation = location
			if stations.count>100 {
				setNearbyStations(location)
			}
		}
	}
	
	public func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
		print(error)
	}
}