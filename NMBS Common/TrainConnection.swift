//
//  TrainConnection.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 13/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation

public class Connection {
	public let startStation: Station
	public let startTime: NSDate
	public let startDelay: NSTimeInterval
	public let startPlatform: String
	
	public let endStation: Station
	public let endTime: NSDate
	public let endDelay: NSTimeInterval
	public let endPlatform: String
	
	public let viaCount: Int
	
	public var realStartTime: NSDate {
		return startTime.dateByAddingTimeInterval(startDelay)
	}
	public var realEndTime: NSDate {
		return endTime.dateByAddingTimeInterval(endDelay)
	}
	
	public init(startStation: Station, startTime: Double, startDelay: NSTimeInterval = 0, startPlatform: String, endStation: Station, endTime: Double, endDelay: NSTimeInterval = 0, endPlatform: String, viaCount: Int = 0) {
		self.startStation = startStation
		self.startTime = NSDate(timeIntervalSince1970: startTime)
		self.startDelay = startDelay
		self.startPlatform = startPlatform
		
		self.endStation = endStation
		self.endTime = NSDate(timeIntervalSince1970: endTime)
		self.endDelay = endDelay
		self.endPlatform = endPlatform
	
		self.viaCount = viaCount
	}
	
	public var endPoints: ConnectionEndpoints {
		return ConnectionEndpoints(start: startStation, end: endStation)
	}
	
	public func hasSameEndpointsAs(connection: Connection) -> Bool {
		return connection.endPoints == endPoints
	}
}

public struct ConnectionEndpoints: Hashable, Equatable {
	public let start: Station
	public let end: Station
	
	public init (start: Station, end: Station) {
		(self.start, self.end) = (start, end)
	}
	
	public var tuple: (Station, Station) {
		return (start, end)
	}
	
	public var hashValue: Int { return (start.id + end.id).hashValue }
}

public func == (left: ConnectionEndpoints, right: ConnectionEndpoints) -> Bool {
	return (left.start, left.end) == (right.start, right.end)
}

public func == (left: ConnectionEndpoints, right: (Station?, Station?)) -> Bool {
	return (left.start, left.end) == (right.0, right.1)
}

public func ==<Element: Equatable> (left: (Element, Element), right: (Element?, Element?)) -> Bool {
	return left.0 == right.0 && left.1 == right.1
}

public func ==<Element: Equatable> (left: (Element, Element), right: (Element, Element)) -> Bool {
	return left.0 == right.0 && left.1 == right.1
}

infix operator ~ {}

func ~(left: Connection, right: Connection) -> Bool {
	return left.hasSameEndpointsAs(right)
}