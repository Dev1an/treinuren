//
//  Station.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 7/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation
import CoreLocation

public class Station: NSObject {
	public let id: String
	public let name: String
	public let location: CLLocation
	
	public init(id: String, name: String, location: CLLocation) {
		(self.id, self.name, self.location) = (id, name, location)
	}

	public required init?(coder aDecoder: NSCoder) {
		id = aDecoder.decodeObjectForKey("id") as! String
		name = aDecoder.decodeObjectForKey("name") as! String
		location = aDecoder.decodeObjectForKey("location") as! CLLocation
	}
	
	override public var description: String {
		return "\(name) (NMBS)"
	}
	
	final public func distanceFrom(location: CLLocation) -> CLLocationDistance {
		return self.location.distanceFromLocation(location)
	}
}

extension Station: NSCoding {
	public func encodeWithCoder(aCoder: NSCoder) {
		[
			"id": id,
			"name": name,
			"location": location
		].forEach {
			aCoder.encodeObject($1, forKey: $0)
		}
	}
}

extension Station: Comparable {}

public func ==(left: Station, right: Station) -> Bool {
	return left.id == right.id
}

public func <(lhs: Station, rhs: Station) -> Bool {
	return lhs.name < rhs.name
}

public func <=(lhs: Station, rhs: Station) -> Bool {
	return lhs.name <= rhs.name
}

public func >(lhs: Station, rhs: Station) -> Bool {
	return lhs.name > rhs.name
}

public func >=(lhs: Station, rhs: Station) -> Bool {
	return lhs.name >= rhs.name
}