//
//  SpeechTestInterfaceController.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 14/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import WatchKit
import Foundation


class SpeechTestInterfaceController: WKInterfaceController {
	@IBOutlet var sampledText: WKInterfaceLabel!
	
	@IBAction func showTextInput() {
		presentTextInputControllerWithSuggestions(nil, allowedInputMode: WKTextInputMode.AllowEmoji) {
			if let text = $0?.first as? String {
				self.sampledText.setText(text)
			}
		}
	}
	
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
