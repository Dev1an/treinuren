//
//  InterfaceController.swift
//  Treinen WatchKit Extension
//
//  Created by Damiaan Dufaux on 7/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import WatchKit
import Foundation
import NmbsWatch

var allConnections = [Connection]()

let stopImageNames = [
	0: "0-stops",
	1: "1-stop",
	2: "2-stops"
]

class InterfaceController: WKInterfaceController {
	@IBOutlet var startPicker: WKInterfacePicker!
	@IBOutlet var destinationPicker: WKInterfacePicker!
	
	@IBOutlet var connectionFieldGroup: WKInterfaceGroup!
	@IBOutlet var loadingText: WKInterfaceLabel!
	
	@IBOutlet var startTimeLabel: WKInterfaceLabel!
	@IBOutlet var endTimeLabel: WKInterfaceLabel!
	@IBOutlet var timeUntilDeparture: WKInterfaceTimer!
	@IBOutlet var startTrackLabel: WKInterfaceLabel!
	
	@IBOutlet var tripDuration: WKInterfaceLabel!
	@IBOutlet var timeUntilArrival: WKInterfaceTimer!
	
	@IBOutlet var tripVisualisation: WKInterfaceImage!
	
	let timeFormatter = NSDateFormatter()
	let formatTime: NSDate -> String

	let durationFormatter = NSDateComponentsFormatter()
	let formatDuration: NSTimeInterval -> String?
	
	var nearbyStations = [Station]() {
		didSet {
			if nearbyStations.count > 0 {
				if startIndex == nil { startIndex = 0 }
			} else {
				startIndex = nil
			}
		}
	}
	var favorites = [Station]() {
		didSet {
			if favorites.count > 0 {
				if endIndex == nil { endIndex = 0 }
			} else {
				endIndex = nil
			}
		}
	}
	var startIndex: Int? {
		didSet {
			setDestinationPickerData()
			getRoutes()
		}
	}
	var endIndex: Int? {
		didSet {
			getRoutes()
		}
	}
	var startPoint: Station? {
		if let index = startIndex {
			return nearbyStations[index]
		} else {
			return nil
		}
	}
	var endPoint: Station? {
		if let index = endIndex {
			return favorites.filter{$0.id != startPoint?.id}[index]
		} else {
			return nil
		}
	}
	
	override init() {
		timeFormatter.dateFormat = "H:mm"
		formatTime = timeFormatter.stringFromDate

		durationFormatter.unitsStyle = .Short
		durationFormatter.allowedUnits = [.Minute, .Hour]
		formatDuration = durationFormatter.stringFromTimeInterval

		super.init()

		nmbsManager.getNearbyStations = { stations in
			self.startPicker.setItems(stations.map { station in
				let item = WKPickerItem()
				item.title = station.name
				return item
			})
			self.nearbyStations = Array(stations)
		}
	}

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        // Configure interface objects here.
	}
	
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
		getRoutes()
		nmbsManager.updateLocation()
		
		nmbsManager.whenStationsAreAvailable{
			self.favorites = readFavoritesFromPreferences()
			self.setDestinationPickerData()
		}

		preferences.addObserver(self, forKeyPath: "favoriteStations", options: .New, context: nil)
   }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
		preferences.removeObserver(self, forKeyPath: "favoriteStations")
    }
	
	func setDestinationPickerData() {
		let destinations: [Station]
		if self.startPoint == nil {
			destinations = favorites
		} else {
			destinations = favorites.filter{$0.id != startPoint!.id}
		}
		destinationPicker.setItems(destinations.map { station in
			let item = WKPickerItem()
			item.title = station.name
			return item
		})
	}
	
	var downloads = [NSURLSessionDataTask]()
	var connectionCache = [ConnectionEndpoints: [Connection]]()
	
	func suspendDownloads() {
		for download in downloads {
			download.suspend()
		}
	}
	
	func resumeDownloads() {
		for download in downloads {
			download.resume()
		}
	}
	
	func getRoutes() {
		if let startPoint = startPoint, endPoint = endPoint {

			let now = NSDate()
			let endpoints = ConnectionEndpoints(start: startPoint, end: endPoint)
			if let connections = connectionCache[endpoints] {
				let futureConnections = connections.filter { $0.startTime > now }
				if futureConnections.count > 0 {
					updateConnectionFields(forConnection: futureConnections[0], cached: true)
				} else {
					connectionCache.removeValueForKey(endpoints)
					showDownloadProgress()
				}
			} else {
				showDownloadProgress()
			}
			suspendDownloads()

			var task: NSURLSessionDataTask! = nil
			task = nmbsManager.getTrains(from: startPoint, to: endPoint) {
				if let connection = ($0.filter { $0.startTime > now }.first) {
					if connection.endPoints == (self.startPoint, self.endPoint) {
						self.updateConnectionFields(forConnection: connection)
						allConnections = $0
					}
					self.connectionCache[connection.endPoints] = $0
				}
				self.downloads.removeFirst(task)
				self.resumeDownloads()
			}
			downloads.append(task)
		} else {
			self.timeUntilDeparture.stop()
		}
	}
	
	func updateConnectionFields(forConnection connection: Connection, cached: Bool = false) {
		connectionFieldGroup.setHidden(false)
		loadingText.setHidden(true)
		
		if let image = stopImageNames[connection.viaCount] {
			tripVisualisation.setImageNamed(image)
		}
		
		startTimeLabel.setText(formatTime(connection.startTime))
		startTrackLabel.setText("Spoor \(connection.startPlatform)")
		timeUntilDeparture.stop()
		timeUntilDeparture.setDate(connection.realStartTime)
		timeUntilDeparture.start()
		endTimeLabel.setText(formatTime(connection.endTime))

		tripDuration.setText(formatDuration(connection.realEndTime.timeIntervalSinceDate(connection.realStartTime)))
		timeUntilArrival.setDate(connection.realEndTime)
		
		if cached {
			timeUntilDeparture.setTextColor(UIColor(hue:0.55, saturation:0.43, brightness:1, alpha:1))
		} else {
			timeUntilDeparture.setTextColor(delayColor(connection.startDelay, limit: 30*60))
		}
	}
	
	func showDownloadProgress() {
		connectionFieldGroup.setHidden(true)
		loadingText.setHidden(false)
	}
	
	@IBAction func selectSource(value: Int) {
		startIndex = value
	}
	
	@IBAction func selectDestination(value: Int) {
		endIndex = value
	}
	
}

extension InterfaceController {
	override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
		if let keyPath = keyPath {
			if let object = object as? NSUserDefaults {
				if keyPath == "favoriteStations" && object == preferences {
					favorites = readFavoritesFromPreferences()
					setDestinationPickerData()
				}
			}
		}
	}
}

extension Array where Element : Equatable {
	mutating func removeFirst(element: Element) {
		if let index = indexOf(element) {removeAtIndex(index)}
	}
}
