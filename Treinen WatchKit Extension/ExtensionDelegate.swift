//
//  ExtensionDelegate.swift
//  Treinen WatchKit Extension
//
//  Created by Damiaan Dufaux on 7/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import WatchKit
import NmbsWatch
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {
	
    func applicationDidFinishLaunching() {
        print("launch")
		
		watchConnection.delegate = self
		watchConnection.activateSession()
		
		if preferences.objectForKey("favoriteStations") == nil {
			watchConnection.sendMessage(["requestFavoriteStations": true], replyHandler: {
				preferences.setObject($0["favoriteStations"], forKey: "favoriteStations")
			}, errorHandler: {
				print("favoriteStationRequestError: \($0)")
			})
		}
    }
	
	func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
		if let favoriteStationIds = applicationContext["favoriteStations"] as? [String] {
			preferences.setObject(favoriteStationIds, forKey: "favoriteStations")
		}
	}

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }

}
