//
//  ConnectionTableRowController.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 21/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import WatchKit

class ConnectionTableRowController: NSObject {
	@IBOutlet var timer: WKInterfaceTimer!
	@IBOutlet var startTime: WKInterfaceLabel!
	@IBOutlet var endTime: WKInterfaceLabel!
	@IBOutlet var duration: WKInterfaceLabel!
	@IBOutlet var viaCount: WKInterfaceLabel!
}
