//
//  TableInterfaceController.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 13/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import WatchKit
import Foundation


class TableInterfaceController: WKInterfaceController {
	@IBOutlet var startLabel: WKInterfaceLabel!
	@IBOutlet var endLabel: WKInterfaceLabel!
	@IBOutlet var table: WKInterfaceTable!
	
	let timeFormatter = NSDateFormatter()
	let formatTime: NSDate -> String

	let durationFormatter = NSDateComponentsFormatter()
	let formatDuration: NSTimeInterval -> String?
	
	override init() {
		timeFormatter.dateFormat = "H:mm"
		formatTime = timeFormatter.stringFromDate

		durationFormatter.unitsStyle = .Short
		durationFormatter.allowedUnits = [.Minute, .Hour]
		formatDuration = durationFormatter.stringFromTimeInterval
		
		super.init()
	}

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
		
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
		
		startLabel.setText(allConnections.first?.startStation.name)
		endLabel.setText(allConnections.first?.endStation.name)
		
		let connections = allConnections.sort {$0.realEndTime < $1.realEndTime}
		
		table.setNumberOfRows(connections.count, withRowType: "ConnectionTableRowController")
		for index in 0 ..< connections.count {
			let row = table.rowControllerAtIndex(index) as! ConnectionTableRowController
			let connection = connections[index]
			row.startTime.setText(formatTime(connection.startTime))
			row.endTime.setText(formatTime(connection.endTime))
			row.viaCount.setText("\(connection.viaCount) overstap\(connection.viaCount==1 ? "" : "pen")")
			
			row.timer.stop()
			row.timer.setDate(connection.realStartTime)
			row.timer.start()
			row.timer.setTextColor(delayColor(connection.startDelay, limit: 1800))
			
			row.duration.setText(formatDuration(connection.endTime.timeIntervalSinceDate(connection.startTime)))
		}
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
