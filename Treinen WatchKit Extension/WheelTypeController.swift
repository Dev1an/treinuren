//
//  WheelTypeController.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 12/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import WatchKit
import Foundation


class WheelTypeController: WKInterfaceController {
	@IBOutlet var wheel: WKInterfacePicker!
	@IBOutlet var label: WKInterfaceLabel!
	
	let letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
	var currentLetter = ""
	var word = ""

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
		wheel.setItems(letters.map {
			let item = WKPickerItem()
			item.title = $0
			return item
		})
		
        // Configure interface objects here.
    }
	@IBAction func reset() {
		word = ""
		label.setText(word)
	}
	
	@IBAction func write() {
		word += currentLetter
		label.setText(word)
	}

	@IBAction func letterChanged(value: Int) {
		currentLetter = letters[value]
	}
	
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
