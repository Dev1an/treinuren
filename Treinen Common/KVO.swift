//
//  KVO.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 13/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation

typealias KVObserver = (kvo: KeyValueObserver, change: [NSObject : AnyObject]?) -> Void

class KeyValueObserver {
	let source: NSObject
	let keyPath: String
	private let observer: KVObserver
	
	init(source: NSObject, keyPath: String, options: NSKeyValueObservingOptions, observer: KVObserver) {
		self.source = source
		self.keyPath = keyPath
		self.observer = observer
		source.addObserver(defaultKVODispatcher, forKeyPath: keyPath, options: options, context: self.pointer)
	}
	
	func __conversion() -> UnsafeMutablePointer<KeyValueObserver> {
		return pointer
	}
	
	private lazy var pointer: UnsafeMutablePointer<KeyValueObserver> = {
		return UnsafeMutablePointer<KeyValueObserver>(Unmanaged<KeyValueObserver>.passUnretained(self).toOpaque())
	}()
	
	private class func fromPointer(pointer: UnsafeMutablePointer<KeyValueObserver>) -> KeyValueObserver {
		return Unmanaged<KeyValueObserver>.fromOpaque(COpaquePointer(pointer)).takeUnretainedValue()
	}
	
	class func observe(pointer: UnsafeMutablePointer<KeyValueObserver>, change: [NSObject : AnyObject]?) {
		let kvo = fromPointer(pointer)
		kvo.observer(kvo: kvo, change: change)
	}
	
	deinit {
		source.removeObserver(defaultKVODispatcher, forKeyPath: keyPath, context: self.pointer)
	}
}


class KVODispatcher : NSObject {
	override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
		KeyValueObserver.observe(UnsafeMutablePointer<KeyValueObserver>(context), change: change)
	}
}

private let defaultKVODispatcher = KVODispatcher()