//
//  preferences.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 13/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation
import WatchConnectivity
#if os(iOS)
	import NmbsTouch
#else
	import NmbsWatch
#endif

let watchConnection = WCSession.defaultSession()
let preferences = NSUserDefaults.standardUserDefaults()
let nmbsManager = Manager()

func readFavoritesFromPreferences() -> [Station] {
	if let favoriteStationIds = preferences.objectForKey("favoriteStations") as? [String] {
		return nmbsManager.stations(WithIds: favoriteStationIds)
	} else {
		return []
	}
}