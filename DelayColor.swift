//
//  File.swift
//  Treinen
//
//  Created by Damiaan Dufaux on 31/12/15.
//  Copyright © 2015 Damiaan Dufaux. All rights reserved.
//

import Foundation
import UIKit

let white = UIColor.whiteColor()
let yellow = UIColor(red: 1, green: 0.93, blue: 0.49, alpha: 1)
let orange = UIColor(red: 1, green: 0.31, blue: 0.06, alpha: 1)
let red = UIColor.redColor()

func + (left: UIColor, right: UIColor) -> UIColor {
	var (lc, rc) = ([CGFloat](count: 4, repeatedValue: 1), [CGFloat](count: 4, repeatedValue: 1))
	left.getRed(&lc[0], green: &lc[1], blue: &lc[2], alpha: &lc[3])
	right.getRed(&rc[0], green: &rc[1], blue: &rc[2], alpha: &rc[3])
	return UIColor(red: lc[0]+rc[0], green: lc[1]+rc[1], blue: lc[2]+rc[2], alpha: 1)
}

let x = white.CGColor


func * (left: CGFloat, right: UIColor) -> UIColor {
	var components = [CGFloat](count: 4, repeatedValue: 1)
	right.getRed(&components[0], green: &components[1], blue: &components[2], alpha: &components[3])
	return UIColor(red: components[0]*left, green: components[1] * left, blue: components[2] * left, alpha: 1)
}

func * (left: UIColor, right: CGFloat) -> UIColor {
	return right * left
}

func interpolateColor(from: UIColor, to: UIColor, position: CGFloat) -> UIColor {
	return (1-position)*from + to*position
}

func delayColor(delay: NSTimeInterval, limit: NSTimeInterval) -> UIColor {
	let position = CGFloat(delay/limit)
	if (position < 0.13) {
		return interpolateColor(white, to: yellow, position: position / 0.13)
	} else if (position < 0.91) {
		return interpolateColor(yellow, to: orange, position: (position-0.13)/0.78)
	} else if (position < 1) {
		return interpolateColor(orange, to: red, position: (position-0.91)/0.9)
	} else {
		return red
	}
}
